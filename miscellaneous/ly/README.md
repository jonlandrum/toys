Leap Year Calculator
====================

This is a simple program that calculates leap years.

###### Usage

    $ ly ([number] ([year]))

Where [number] and [year] are positive integers representing the [number] of leap years to return starting from [year]. Both [number] and [year] are optional; if only one is given, it is interpreted as [number]. For example:

    $ ly 20 1970

would return the 20 leap years following 1970,

    $ ly 20

would return the next 20 leap years following the current year, and

    $ ly

would return the next leap year following the current year.

If the current year is a leap year, a message is displayed noting that fact before the remainder of the output is given.
