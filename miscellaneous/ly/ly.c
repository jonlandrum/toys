#include <stdio.h>
#include <time.h>

int main (int argc, char** argv) {
    int c = 0;
    int max = 1;
    int y;
    if (argc < 3) {
        time_t current = time(NULL);
        struct tm *strTime = localtime(&current);
        y = strTime->tm_year + 1900;
        if (argc == 2) {
            if (!strcmp(argv[1], "-h") || !strcmp(argv[1], "--help") || !strcmp(argv[1], "?")) {
                printf ("Leap Year Calculator\n\n");
                printf ("Usage:\n");
                printf ("\t~ %s ([number] ([year]))\n", argv[0]);
                printf ("  where [number] and [year] are positive integers. Both [number] and [year] are optional; if only one is given, it is interpreted as [number].\n\n");
                printf ("For example:\n");
                printf ("\t~ %s 20 1970\n", argv[0]);
                printf ("  would return the 20 leap years following 1970,\n");
                printf ("\t~ %s 20\n", argv[0]);
                printf ("  would return the next 20 leap years following the current year, and\n");
                printf ("\t~ %s\n", argv[0]);
                printf ("  would return the next leap year following the current year.\n\n");
                printf ("If the current year is a leap year, a message is displayed noting that fact before the remainder of the output is given.\n");
                return 0;
            } else { max = atoi(argv[1]); }
        }
        if ((y % 400 == 0) || (y % 4 == 0 && y % 100 != 0)) {
            printf ("This is a leap year. ");
        }
        if (max > 1) { printf ("The next %d leap years are:\n", max); }
        else { printf ("The next leap year is "); }
    } else {
        max = atoi(argv[1]);
        y   = atoi(argv[2]);
        if ((y % 400 == 0) || (y % 4 == 0 && y % 100 != 0)) {
            printf ("%d is a leap year. ", y);
        }
        if (max > 1) { printf ("The next %d leap years following %d are:\n", max, y); }
        else { printf ("The next leap year following %d is ", y); }
    }
    while (c < max) {
        y++;
        if ((y % 400 == 0) || (y % 4 == 0 && y % 100 != 0)) {
            if (max == 1) { printf ("%d.\n", y); }
            else {
                if ((c + 1) < 10) { printf (" %d.) %d\n", c + 1, y); }
                else { printf ("%d.) %d\n", c + 1, y); }
            }
            c++;
        }
    }
    return 0;
}