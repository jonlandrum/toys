#include <stdio.h>
#include <string.h>

char rot(char in, int rot);

int main(int argc, char** argv) {
    if (argc > 1) {
        int i;
        for (i = 1; i < argc; ++i) {
            int j;
            for (int j = 0; j < strlen(argv[i]); ++j) {
                printf("%c", rot(argv[i][j], 12));
            }
            printf(" ");
        }
        printf("\n");
    }
}

char rot(char in, int rot) {
    /* assume only letters */
    if ((int) in < 65 ||
        (int) in > 122 ||
        (int) in > 90 &&
        (int) in < 97) return in;
    int n = (int) in;
    n -= 65;
    
    /* check if lower-case */
    int lc = 0;
    if (n > 25) {
        lc = 1;
        n -= 32;
    }

    /* calculate modulus */
    n = (n + rot) % 26;

    /* convert back to letters */
    n += 65;
    if (lc) n += 32;

    return (char) n;
}
