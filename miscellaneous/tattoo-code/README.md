Tattoo Code
===========

When I found this image of a C program tattoo:

![Code tattoo](code-tattoo.jpg "Code tattoo")

I knew I had to write it. The output is to be expected. It turns the word "Hate" into "love". But still, `++coolPoints` to whomever added this to their body.
