Miscellaneous
=============

These programs are actual toys, things I've made for a specific purpose, out of
curiosity, or that just don't fit elsewhere.
