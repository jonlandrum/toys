/*
 *	Programmer:  Jonathan Landrum
 *	Date:        
 *	
 *	Program:     template.cpp	
 *	Purpose:     Template.
 *	Assumptions: None.
 */

#include <iostream>

using namespace std;

// --------------------------------------------------
// main():
// --------------------------------------------------
int main () {
	
	// Variables
	
	
	// Introduce the program
	cout << endl;
	cout << "---------------------" << endl;
	cout << "-    Hello World    -" << endl;
	cout << "---------------------" << endl;
	cout << endl;
	
	// Main processing loop
	
	
	// Return the results
	
	
	return (0);
} // End main
