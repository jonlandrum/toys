List Framer
===========

This program takes a list and builds a frame around it in asterisks.

######Usage######

    ~> ./frame [elements]

where [elements] is a space-delimeted list of elements to print. For example:

    ~> ./frame Hello, world!

would return:

    **********
    * Hello, *
    * world! *
    **********
