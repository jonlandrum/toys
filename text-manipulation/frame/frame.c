#include <stdio.h>
#include <string.h>

int main (int argc, char** argv) {
    if (argc < 2 || !strcmp(argv[1], "-h") || !strcmp(argv[1], "--help") || !strcmp(argv[1], "?")) {
        printf ("List Framer\n\n");
        printf ("This program takes a list and builds a frame around it.\n\n");
        printf ("Usage:\n");
        printf ("\t~> %s [elements]\n", argv[0]);
        printf ("where [elements] is a space-delimeted list of elements to print.\n\n");
        printf ("For example:\n");
        printf ("\t~> %s Hello, world!\n", argv[0]);
        printf ("would return:\n");
        printf ("**********\n");
        printf ("* Hello, *\n");
        printf ("* world! *\n");
        printf ("**********\n");
        return 0;
    } else {
        int c, i;
        int len = argc - 1;
        int max = 0;
        char* list[len];
        for (c = 0; c < len; ++c) {
            list[c] = argv[c + 1];
        }
        for (c = 0; c < len; ++c) {
            if (strlen(list[c]) > max) {
                max = strlen(list[c]);
            }
        }
        max += 4;
        for (c = 0; c < max; ++c) {
            printf ("*");
        }
        printf ("\n");
        for (c = 0; c < len; ++c) {
            printf ("* ");
            printf ("%s", list[c]);
            if (strlen(list[c]) < max - 4) {
                for (i = 0; i < max - strlen(list[c]) - 4; ++i) {
                    printf (" ");
                }
            }
            printf (" *\n");
        }
        for (c = 0; c < max; ++c) {
            printf ("*");
        }
        printf ("\n");
    }
}