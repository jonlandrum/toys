import java.util.ArrayList;
import java.util.Arrays;

public class DisplayAbsentValues {
    public static void main(String[] args) {
        System.out.println();
        
        // Array 1
        int[] arr1 = {0, 1, 2, 50, 52, 75};
        PrintArray(arr1);
        System.out.println(DisplayAbsentValues(arr1));
        System.out.println();
        
        // Array 2
        int[] arr2 = {1, 2, 50, 53, 75, 99};
        PrintArray(arr2);
        System.out.println(DisplayAbsentValues(arr2));
        System.out.println();
        
        // Array 3
        int[] arr3 = {2, 50, 52, 75, 98};
        PrintArray(arr3);
        System.out.println(DisplayAbsentValues(arr3));
        System.out.println();
        
        // Array 4
        int[] arr4 = {10, 20, 30, 40, 50};
        PrintArray(arr4);
        System.out.println(DisplayAbsentValues(arr4));
        System.out.println();
        
        // Array 5
        int[] arr5 = {10, 20, 30, 40, 50, 98, 99};
        PrintArray(arr5);
        System.out.println(DisplayAbsentValues(arr5));
        System.out.println();
        
        // Array 6
        int[] arr6 = {10, 20, 30, 40, 50, 97, 99};
        PrintArray(arr6);
        System.out.println(DisplayAbsentValues(arr6));
        System.out.println();
        
        // Array 7
        int[] arr7 = {10, 20, 30, 40, 50, 96, 99};
        PrintArray(arr7);
        System.out.println(DisplayAbsentValues(arr7));
        System.out.println();
        
        // Array 8
        int[] arr8 = {10, 20, 30, 40, 50, 95, 99};
        PrintArray(arr8);
        System.out.println(DisplayAbsentValues(arr8));
        System.out.println();
        
        // Array 9
        int[] arr9 = {0, 1, 10, 20, 30, 40, 50};
        PrintArray(arr9);
        System.out.println(DisplayAbsentValues(arr9));
        System.out.println();
        
        // Array 10
        int[] arr10 = {0, 2, 10, 20, 30, 40, 50};
        PrintArray(arr10);
        System.out.println(DisplayAbsentValues(arr10));
        System.out.println();
        
        // Array 11
        int[] arr11 = {0, 3, 10, 20, 30, 40, 50};
        PrintArray(arr11);
        System.out.println(DisplayAbsentValues(arr11));
        System.out.println();
        
        // Array 12
        int[] arr12 = {0, 4, 10, 20, 30, 40, 50};
        PrintArray(arr12);
        System.out.println(DisplayAbsentValues(arr12));
    }
    
    public static void PrintArray(int[] input) {
        System.out.print("{");
        for (int i = 0; i < input.length; ++i) {
            System.out.print(input[i] + ", ");
        }
        System.out.println("\b\b}");
    }
    
    public static String DisplayAbsentValues(int[] input) {
        String result = "";
        int length;
        for (length = 1; length < input.length; ++length) {
            // This breaks out of the loop one pass too early, so increment length afterward
            if (length == input.length - 1 || input[length + 1] == 0) break;
        }
        length++;
        
        ArrayList<Integer> absent = new ArrayList<Integer>();
        for (int ctr = 0; ctr < 100; ++ctr) {
            if (Arrays.binarySearch(input, ctr) < 0) {
                absent.add(ctr);
            }
        }
        for (int ctr = 0; ctr < absent.size(); ++ctr) {
            if ((ctr < (absent.size() - 2)) && (absent.get(ctr + 2) == absent.get(ctr) + 2)) {
                result += absent.get(ctr);
                while ((ctr < (absent.size() - 2)) && (absent.get(ctr + 2) == absent.get(ctr) + 2)) {
                    ctr++;
                }
                result += "-" + absent.get(ctr + 1);
                ctr++;
            } else {
                result += absent.get(ctr);
            }
            if (ctr < absent.size() - 1) {
                result += ",";
            }
        }
      return result;
    }
}