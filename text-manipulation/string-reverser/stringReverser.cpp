/*
 *	Programmer:  Jonathan Landrum
 *	Date:        25 February 2012
 *	
 *	Program:     stringReverser.cpp	
 *	Purpose:     Takes string input and reverses it
 *	Assumptions: None.
 */

#include <iostream>
#include <string>
using namespace std;

// --------------------------------------------------
// main():
// --------------------------------------------------
int main () {
	
	// Variables
	int counter;
	int counter2;
	string input;
	
	// Introduce the program
	cout << endl;
	cout << "-----------------------------" << endl;
	cout << "-    C++ String Reverser    -" << endl;
	cout << "-----------------------------" << endl;
	cout << endl;
	cout << "This program takes a string" << endl;
	cout << "from the user and reverses it." << endl;
	cout << endl;
	
	// Main processing loop
	cout << "Enter a string to reverse:" << endl;
	
	getline(cin, input);
	char array[input.length()];
	
	// Push the string into an array backwards
	counter2 = 0;
	for (counter = input.length() - 1; counter >= 0; --counter) {
		array[counter2] = input[counter];
		++counter2;
	} // End for
	
	for (counter = 0; counter < input.length(); ++counter) {
		cout << array[counter];
	} // End for
	
	cout << endl;
	
	// Return the results
	
	return (0);
} // End main
