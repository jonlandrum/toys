String Reverser
===============

There are two types of string reversal programs: the ones that reverse the words, and the ones that reverse the characters. This program is one of the latter. It takes a string as an array of characters and reverses it:

    $ Enter a string to reverse:
    $ Hello, world!
    $ !dlrow ,olleH
