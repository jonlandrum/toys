#include <stdio.h>

int main() {
    int start = 32;
    int even = 32;
    int spaces = 49;

    while (even < 127) {
        for (int i = 0; i < spaces; ++i)        printf(" ");
        for (int i = start; i < even; ++i)      printf("%c", i);
                                                printf("%c", even);
        for (int i = even - 1; i >= start; --i) printf("%c", i);
                                                printf("\n");
        start++;
        even += 2;
        spaces--;
    }
}
