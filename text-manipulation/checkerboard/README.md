Checkerboard Printer
====================

This was a programming challenge I dug up online. The idea is to take a Natural number from the user and print a checkerboard of that size like so:

![Example checkerboard](checkerboard.png "Example checkerboard")
