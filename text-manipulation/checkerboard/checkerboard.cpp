/*
 *	Programmer:  Jonathan Landrum
 *	Date:        24 February 2012
 *	
 *	Program:     checkerboard.cpp	
 *	Purpose:     Prints a checkerboard of
 *	             user-defined size.
 *	Assumptions: None.
 *
 */

#include <iostream>

using namespace std;

// --------------------------------------------------
// main():
// --------------------------------------------------
int main () {
	
	// Variables
	int counter, iterator, input;
	
	// Introduce the program
	cout << endl;
	cout << "----------------------------------" << endl;
	cout << "-    C++ Checkerboard Printer    -" << endl;
	cout << "----------------------------------" << endl;
	cout << endl;
	cout << "             +-----+" << endl;
	cout << "             |     |" << endl;
	cout << "             |     |" << endl;
	cout << "             |     |" << endl;
	cout << "             +-----+" << endl;
	cout << endl;
	cout << "This program takes user input of an" << endl;
	cout << "integer and prints a checkerboard" << endl;
	cout << "of that size." << endl;
	
	cout << "What size checkerboard do you want: ";
	cin  >> input;
	
	// Main processing loop
	for (counter = 0; counter < input; ++counter) {
		for (iterator = 0; iterator < input; ++iterator) {
			cout << "+-----";
		}
		cout << "+" << endl;
		for (iterator = 0; iterator < input; ++iterator) {
			cout << "|     ";
		}
		cout << "|" << endl;
		for (iterator = 0; iterator < input; ++iterator) {
			cout << "|     ";
		}
		cout << "|" << endl;
		for (iterator = 0; iterator < input; ++iterator) {
			cout << "|     ";
		}
		cout << "|" << endl;
	} // End for
	
	for (iterator = 0; iterator < input; ++iterator) {
		cout << "+-----";
	}
	cout << "+" << endl;
	
	// Return the results
	
	
	return (0);
} // End main
