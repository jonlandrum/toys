Delimeters
==========

This program checks if a given string has its delimeters (`[]{}()`) properly closed and nested.
