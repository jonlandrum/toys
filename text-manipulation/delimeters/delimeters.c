#include <stdio.h>

int main (int argc, char **argv) {
    if (argc < 2) {
        printf ("Usage:\n");
        printf ("\t~> %s [string]\n", argv[0]);
        printf ("where [string] is a string to check.\n");
    } else {
        char *ptr = argv[1];
        char *tmp = ptr;
        while (*ptr) {
            if (*ptr == '(' || *ptr == '[' || *ptr == '{') {
                tmp = ptr;
                while (*tmp) {
                    if (*tmp == ')' || *tmp == ']' || *tmp == '}') {
                        if ((*ptr == '(' && *tmp == ')') ||
                            (*ptr == '[' && *tmp == ']') ||
                            (*ptr == '{' && *tmp == '}')) {
                            break;
                        } else {
                            printf ("Not valid.\n");
                            return 0;
                        }
                    }
                    tmp++;
                }
            }
            ptr++;
        }
    }
    printf ("Valid.\n");
    return 0;
}