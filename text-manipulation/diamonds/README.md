Diamonds
========

This is a collection of programs that explore the time-honored tradition of requiring freshmen computer science students to write a program that prints a diamond to the terminal.

Included in this collection is [the typical filled diamond of asterisks](solid-diamond.cpp "View this source code"):

      *
     ***
    *****
     ***
      *

A [hollow version of the same](hollow-diamond.cpp "View this source code"):

      *
     * *
    *   *
     * *
      *

And a neat [character diamond implementation](char-diamond.cpp "View this source code"):

      A
     B B
    C   C
     B B
      A

I stumbled upon the [character diamond challenge](http://www.craigmurphy.com/blog/?p=1417 "The Social Programmer: Programming Challenge!") one day while searching for programming assignments, and found this image:

![Character Diamond Challenge](diamond.jpg "Character Diamond Challenge")

This was a programming assignment from the late '80's that was intended to be programmed in Pascal. It was posted online in early 2009, well before all the chic "hacker" websites existed that have weekly challenges nowadays. It's much easier with these sites to find things to do when you're bored. Before that, though, you had to dig. I'm glad I went digging.

######Screenshot######
![Finished product](char-diamond-results.png "Finished product")
