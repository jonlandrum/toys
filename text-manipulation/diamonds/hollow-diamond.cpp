// ==================================================
// Programmer:  Jonathan Landrum
// Date:        4 February 2012
// ==================================================
// Program:     hollow-diamond.cpp
// Purpose:     Takes user input of one integer and
//              returns a diamond of that size.
// Assumptions: 1.) Input should be an odd integer:
//                  - Real input is rounded down.
//                  - Even input is incremented.
// ==================================================

#include <iostream>
#include <string>

using namespace std;

// --------------------------------------------------
// main():
// Does ALL the things!
// --------------------------------------------------
int main () {
	
	// ------------------------------------------
	// Declare variables, not war
	// ------------------------------------------
	int input;
	int spaces;
	int counter;
	
	// ------------------------------------------
	// Introduce the program
	// ------------------------------------------
	cout << endl;
	cout << "---------------------------" << endl;
	cout << "-      Hollow Diamond     -" << endl;
	cout << "---------------------------" << endl;
	cout << endl;
	cout << "             *" << endl;
	cout << "            * *" << endl;
	cout << "           *   *" << endl;
	cout << "          *     *" << endl;
	cout << "         *       *" << endl;
	cout << "          *     *" << endl;
	cout << "           *   *" << endl;
	cout << "            * *" << endl;
	cout << "             *" << endl;
	cout << endl;
	cout << "This program asks for an" << endl;
	cout << "integer, and returns a" << endl;
	cout << "diamond of that size." << endl;
	cout << endl;
	
	// ------------------------------------------
	// Main processing loop
	// ------------------------------------------
	cout << "Enter a number for the size of your diamond: ";
	cin  >> input;
	
	if (input % 2 == 0)
		input++;
	counter = 1;
	
	while (counter < input) {
		spaces = (input - counter) / 2;
		for (int c = 0; c < spaces; c++)
			cout << " ";
		
		cout << "*";
		
		if (counter == 1)
			spaces = 0;
		else
			spaces = counter - 2;
		
		for (int i = 0; i < spaces; i++)
			cout << " ";
		
		if (counter != 1)
			cout << "*" << endl;
		else
			cout << endl;
		
		counter += 2;
	} // End while
	while (counter > 0) {
		spaces = (input - counter) / 2;
		for (int j = 0; j < spaces; j++)
			cout << " ";
		
		cout << "*";
		
		if (counter == 1)
			spaces = 0;
		else
			spaces = counter - 2;
		
		for (int n = 0; n < spaces; n++)
			cout << " ";
		
		if (counter != 1)
			cout << "*" << endl;
		else
			cout << endl;
		
		counter -= 2;
	} // End while
	
	return (0);
} // End main
