// ==================================================
// Programmer:  Jonathan Landrum
// Date:        4 February 2012
// ==================================================
// Program:     char-diamond.cpp
// Purpose:     Takes user input of one character and
//              returns a diamond of that size.
// Assumptions: 1.) Input is a single letter (char).
//                  Any other input will return un-
//                  expected results.
// ==================================================

#include <iostream>
#include <string>

using namespace std;

// --------------------------------------------------
// Global Variables
// --------------------------------------------------
char letter[26] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
	           'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};

// --------------------------------------------------
// main():
// Does ALL the things!
// --------------------------------------------------
int main () {
	
	// ------------------------------------------
	// Declare variables, not war
	// ------------------------------------------
	char in;
	int  counter;
	int  input;
	int  level;
	int  spaces;
	
	// ------------------------------------------
	// Introduce the program
	// ------------------------------------------
	cout << endl;
	cout << "---------------------------" << endl;
	cout << "-    Character Diamond    -" << endl;
	cout << "---------------------------" << endl;
	cout << endl;
	cout << "             A" << endl;
	cout << "            B B" << endl;
	cout << "           C   C" << endl;
	cout << "          D     D" << endl;
	cout << "         E       E" << endl;
	cout << "          D     D" << endl;
	cout << "           C   C" << endl;
	cout << "            B B" << endl;
	cout << "             A" << endl;
	cout << endl;
	cout << "This program asks for a" << endl;
	cout << "character, and returns a" << endl;
	cout << "diamond of that size." << endl;
	cout << endl;
	
	// ------------------------------------------
	// Main processing loop
	// ------------------------------------------
	cout << "Enter a single letter for the size of your diamond: ";
	cin  >> in;
	
	input = toupper(in) - 64;
	input = input * 2 - 1;
	counter = 1;
	level = 0;
	
	while (counter < input) {
		if (input % 2 == 0)
			spaces = ((input - counter) / 2) + 1;
		else
			spaces = (input - counter) / 2;
		
		for (int c = 0; c < spaces; c++)
			cout << " ";
		
		cout << letter[level];
		
		if (counter == 1)
			spaces = 0;
		else
			spaces = counter - 2;
		
		for (int i = 0; i < spaces; i++)
			cout << " ";
		
		if (counter != 1)
			cout << letter[level] << endl;
		else
			cout << endl;
		
		counter += 2;
		level++;
	} // End while
	while (counter > 0) {
		if (input % 2 == 0)
			spaces = ((input - counter) / 2) + 1;
		else
			spaces = (input - counter) / 2;
		
		for (int j = 0; j < spaces; j++)
			cout << " ";
		
		cout << letter[level];
		
		if (counter == 1)
			spaces = 0;
		else
			spaces = counter - 2;
		
		for (int n = 0; n < spaces; n++)
			cout << " ";
		
		if (counter != 1)
			cout << letter[level] << endl;
		else
			cout << endl;
		
		counter -= 2;
		level--;
	} // End while
	
	return (0);
} // End main
