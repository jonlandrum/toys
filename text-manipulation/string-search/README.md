String Search
=============

This program searches for the first occurrence of the given substring in the given file.

######Usage######

    $ ./search [file] [string]

where [file] is the ASCII file to search and [string] is the string to search for.
