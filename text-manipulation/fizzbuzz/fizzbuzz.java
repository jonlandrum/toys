// ****************************************************
// Programmer:           Jonathan Landrum
// Date:                 29 October 2011
// Class:                Comp. Sci. 101 (Seriously)
// ****************************************************
// Program:              fizzBuzz.java
// Purpose:              To prove that I can program.
// Assumptions:          A lot, apparently.
// ****************************************************

import java.util.Scanner;

public class fizzBuzz {
	public static void main (String[] args) {
		
		// --------------------------------------------
		// Variables
		// --------------------------------------------
		int     c = 1;
		String  response;
		Scanner scan = new Scanner(System.in);
		
		// --------------------------------------------
		// Introduce the program
		// --------------------------------------------
		System.out.println ("-------------------------------------");
		System.out.println ("-             Fizz Buzz             -");
		System.out.println ("-------------------------------------");
		System.out.println ();
		System.out.println ("This program reproduces a traditional");
		System.out.println ("British children's counting game, but");
		System.out.println ("more specifically, it represents a");
		System.out.println ("coding challenge.");
		System.out.println ();
		System.out.print   ("Would you like to continue? [Y/N] ");
		response = scan.nextLine();
		
		// --------------------------------------------
		// Main block
		// --------------------------------------------
		while (response.charAt(0) == 'y' || response.charAt(0) == 'Y') {
			while (c <= 100) {
				if (c > 3) {
					if (c % 3 == 0 && c % 5 == 0) {
						System.out.println ("FizzBuzz");
					} else if (c % 3 == 0 && c % 5 == 1) {
						System.out.println ("Fizz");
					} else if (c % 3 == 1 && c % 5 == 0) {
						System.out.println ("Buzz");
					} else {
						System.out.println (c);
					} // End test if block
				} else {
					System.out.println (c);
				} // End print if block
				c++;
			} // End main while block
			System.out.println ("-------------------------------------");
			System.out.println ("Process complete.");
			System.out.print   ("Would you like to continue? [Y/N] ");
			response = scan.nextLine();
		} // End while
		
		System.out.println ();
		System.out.println ("\\\\//_ Live long and prosper.");
	} // End main
} // End fizzBuzz
