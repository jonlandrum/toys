Fizz Buzz
=========

This is the saddest program I've ever written. I was reading Jeff Atwood's blog one day when I happened upon the article ``<a href="http://blog.codinghorror.com/why-cant-programmers-program/" title="Why Can't Programmers.. Program?">Why Can't Programmers.. Program?</a>'' It struck me with a horrible feeling. This is literally a CS 101 problem, something you learn to solve in your first semester. I wrote this, not because it was a great challenge, but because I sort of had to, because it sickens me that people with degrees in Computer Science cannot do this.

I'm including my original Java version (written while a freshman, no less!), a C one-liner (save the obligatory include statement), a Ruby implementation that clocks in at just 82 characters, and even a Common Lisp version, just for kicks and giggles.
