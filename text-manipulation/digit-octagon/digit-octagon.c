#include <stdio.h>

int main() {
    int itr;
    int spc = 7;
    int mid = 1;
    int bdr = 1;
    int swt = 0;
    
    for (int x = 0; x < 22; ++x) {
        for (itr = 0; itr < spc; ++itr)
            printf(" ");
        while (bdr < mid)
            printf("%d", bdr++);
        for (itr = 0; itr < 8; ++itr)
            printf("%d", mid);
        while (bdr > mid || bdr > 1)
            printf("%d", --bdr);
        printf("\n");
        if (mid < 8 && swt == 0) {
            mid++;
            spc--;
        } else swt++;
        if (swt > 7) {
            mid--;
            spc++;
        }
    }
}
