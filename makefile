#
# Makefile for foo
# This is an example makefile; modify as necessary
#

CC=g++
CFLAGS=-g -Wall
all: foo

foo: foo.cpp
	$(CC) $(CFLAGS) -o foo foo.cpp

clean:
	rm foo
