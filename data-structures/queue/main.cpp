#include "queue.h"


int main() {
	int   count;
	int   item;
	queue myQueue;
	
	cout << "--------------------------------------------------------\n\n";
	cout << "               Programming a Queue in C++\n\n";
	cout << "--------------------------------------------------------\n\n";
	
	cout << "This program simulates a waiting line -- also called a\n";
	cout << "queue -- using a queue data structure.\n\n";
	cout << "This program only handles integers at the moment.\n";
	cout << "";
	
	for (count = 0; count < SIZE; ++count) {
		cout << "Entry " << count + 1 << ": ";
		cin  >> item;
		myQueue.enqueue(item);
		cout << "Items in the list: " << myQueue.size() << endl;
	}
	
	cout << "\nNow to dequeue those items:\n" << endl;
	
	for (count = 0; count < SIZE; ++count) {
		cout << "Entry " << count + 1 << ": " << myQueue.dequeue() << endl;
	}
	
	return (0);
}