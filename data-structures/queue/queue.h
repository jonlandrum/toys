#include <cstdlib>
#include <iostream>
#include <string>

#define SIZE 10

using namespace std;

class queue {
private:
	/* Instance Variables */
	int  count;
	int  front;
	int  rear;
public:
	/* Constructor */
	queue();
	
	/* Instance Methods */
	bool isEmpty();
	void enqueue(const int item);
	int  dequeue();
	int  first();
	int  size();
};