#include "queue.h"


int data[SIZE];

queue::queue() {
	count  = 0;
	front  = 0;
	rear   = 0;
	
	for (int i = 0; i < SIZE; ++i)
		data[i] = 0.0;
}

bool queue::isEmpty() {
	bool result;
	
	if (count == 0)
		result = true;
	else
		result = false;
	
	return result;
}

void queue::enqueue(const int item) {
	if (count < SIZE) {
		data[rear] = item;
		rear = (rear + 1) % SIZE;
		++count;
	} else
		std::cerr << "Error: Attempt to enqueue to full queue\n";
	
}

int queue::dequeue() {
	int result = 0;
	
	if (!isEmpty()) {
		result = data[front];
		data[front] = NULL;
		front = (front + 1) % SIZE;
		--count;
	} else
		std::cerr << "Error: Attempt to dequeue from an empty queue\n";
	
	return result;
}

int queue::first() {
	return data[front];
}

int queue::size() {
	return count;
}