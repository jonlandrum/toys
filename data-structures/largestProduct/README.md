Largest Product
===============

This program was a challenge I found online to find the largest product of five consecutive digits of a 1,000-digit number. Since that is an impossible number to store in C++, and since that is the language I wanted to use, I opted to break the number up and store it in an array. This program surely gives the correct result, but a ``better'' solution would be to program this in a language that allows arbitrarily long integers, such as Lisp, or to use a big number library, such as GMP.
