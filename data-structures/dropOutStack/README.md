Drop-out Stack
==============

This program illustrates one way of accomplishing a drop-out stack, the data structure used in places such as undo buttons that keep the last *n* items in a list.
