#include <iostream>
#define SIZE 10
using namespace std;


struct stack {
	int count;
	int data[SIZE];
};

inline void stack_init(struct stack & the_stack) {
	the_stack.count = 0;
}

inline void stack_dropOut(struct stack & the_stack) {
	for (int c = 0; c < SIZE; ++c)
		the_stack.data[c] = the_stack.data[c + 1];
	
	--the_stack.count;
}

inline bool stack_isEmpty(struct stack & the_stack) {
	return (the_stack.count == 0);
}

inline void stack_push(struct stack & the_stack, const int item) {
	if (the_stack.count == SIZE)
		stack_dropOut(the_stack);
	
	the_stack.data[the_stack.count] = item;
	++the_stack.count;
}

inline int stack_pop(struct stack & the_stack) {
	if (stack_isEmpty(the_stack)) {
		cout << "Error: Empty Collection Exception" << endl;
		return(-1);
	}
	
	--the_stack.count;
	return (the_stack.data[the_stack.count]);
}

inline int stack_peek(struct stack & the_stack) {
	return (the_stack.data[the_stack.count]);
}

inline bool stack_size(struct stack & the_stack) {
	return (the_stack.count);
}

inline void stack_toString(struct stack & the_stack) {
	for (int c = 0; c < the_stack.count; ++c)
		cout << "Item " << c + 1 << ": " << the_stack.data[c] << endl;
}