#include "stack.h"

int main() {
	stack myStack;
	stack_init(myStack);
	int item;
	
	// Demonstrate the dropout
	for (int c = 0; c < 15; ++c) {
		cout << "Enter a number to the stack: ";
		cin  >> item;
		stack_push(myStack, item);
		stack_toString(myStack);
	}
	
	cout << endl;
	
	// Demonstrate an empty collection exception
	for (int c = 0; c < 11; ++c) {
		stack_pop(myStack);
		stack_toString(myStack);
	}
	
	return (0);
	
}