/**
 * ElementNotFoundException represents the situation in which a target 
 * element is not present in a collection
 *
 * @author Dr. Lewis
 * @author Dr. Chase
 * @version 1.0, 08/13/08
 */

public class ElementNotFoundException extends RuntimeException {
	public ElementNotFoundException (String collection) {
		System.out.println ("The target element is not in this collection");
	}
}