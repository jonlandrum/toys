AVL Tree
========

This is a Java implementation of a <a href="http://en.wikipedia.org/wiki/Associative_array" title="Article on WikiPedia">Dictionary</a> with an <a href="http://en.wikipedia.org/wiki/AVL_tree" title="Article on WikiPedia">AVL tree</a> as the underlying structure.
