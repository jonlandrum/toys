/**
 * Mine, except for the iteratorInOrder and inorder methods,
 * which I took from LinkedBinaryTree to do the toString() printout.
 */

import java.util.Iterator;

public class Dictionary<T> extends AVLTree<T> implements DictionaryADT<T> {
    /**
     * Constructor
     */
    public Dictionary () {
        super();
    }
    /**
     * findEntry:
     * Returns a String.
     * Searches for specified element in the dictionary.
     */
    public String findEntry (T key) {
        Node<T> temp = this.find(key);
        return temp.toString();
    }
    /**
     * insertEntry:
     * Returns void.
     * Inserts the specified element with the specified key into the dictionary.
     */
    public void insertEntry (T key, T element) {
        this.insert(key, element);
    }
    /**
     * removeEntry:
     * Returns a String.
     * Searches for and removes the specified element from the dictionary, and returns its value.
     */
    public String removeEntry (T key) {
        Node<T> temp = this.find(key);
        System.out.println (temp.toString());
        this.remove(temp);
        return ("");
    }
    /**
     * toString:
     * Returns a String.
     * Gives a printout of the tree.
     */
    public String toString () {
        String result = "";
        Iterator<T> iterator = iteratorInOrder();
        while (iterator.hasNext()){
            result +=  (iterator.next() + "\n");
        } 
        return result;
    }
}