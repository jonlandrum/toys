/**
 * Author: Jonathan Landrum
 * 
 * Defines the interface to a Dictionary data structure
 */
public interface DictionaryADT<T> {
    /**
     * findEntry:
     * Returns type Object. Searches for specified element in the dictionary.
     */
    public String findEntry (T key);
    /**
     * insertEntry:
     * Returns void. Inserts the specified element with the specified key into the dictionary.
     */
    public void insertEntry (T key, T element);
    /**
     * removeEntry:
     * Returns type Object. Searches for and removes the specified element from the dictionary, and returns its value.
     */
    public String removeEntry (T key);
}