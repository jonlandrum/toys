/**
 * Driver for Dictionary implemented via an AVL tree.
 * 
 * @author Jonathan Landrum
 */

import java.util.Scanner;

public class Driver {
    /**
     * Main:
     * Initiates a Scanner object and a Dictionary object,
     * and puts the user in a loop for input.
     */
    public static void main (String[] args) {
        String input = "", term, definition;
        Scanner scan = new Scanner(System.in);
        Dictionary dictionary = new Dictionary();
        
        /*
         * Introduce the program
         */
        System.out.println ("=================================================");
        System.out.println ();
        System.out.println ("Java Ordered Dictionary Utilized With An AVL Tree");
        System.out.println ();
        System.out.println ("=================================================");
        System.out.println ();
        
        /*
         * Start a loop for user input
         */
        while (!input.equalsIgnoreCase("X")) {
            System.out.println ("Please select from the following choices:");
            System.out.println ();
            System.out.println ("   --------------------");
            System.out.println ("   A.......Add an entry");
            System.out.println ("   D....Delete an entry");
            System.out.println ("   L....Lookup an entry");
            System.out.println ("   S...Show all entries");
            System.out.println ("   X...............Exit");
            System.out.println ("   --------------------");
            System.out.println ();
            System.out.print   ("Enter a selection: ");
            input = scan.nextLine  ();
            
            // Add
            if (input.equalsIgnoreCase("A")) {
                System.out.println ();
                System.out.print ("Enter the term to be defined: ");
                term = scan.nextLine ();
                System.out.print ("Enter the definition for \"" + term + "\": ");
                definition = scan.nextLine ();
                
                dictionary.insertEntry (term, definition);
                System.out.println ();
                System.out.println ("Added:");
                System.out.println ("     " + term + " " + definition);
                System.out.println ();
                
                input = "";
                term = "";
                definition = "";
            }
            // Delete
            if (input.equalsIgnoreCase("D")) {
                System.out.println ();
                System.out.print ("Enter the term to remove: ");
                term = scan.nextLine ();
                
                System.out.println ("Removed:");
                System.out.print   ("     ");
                System.out.print   (dictionary.removeEntry (term));
                System.out.println ("\n");
                
                input = "";
                term = "";
            }
            // Lookup
            if (input.equalsIgnoreCase("L")) {
                System.out.println ();
                System.out.print ("Enter the term to find: ");
                term = scan.nextLine ();
                
                System.out.println ("Found:");
                System.out.print   ("     ");
                System.out.print   (dictionary.findEntry (term));
                System.out.println ();
                
                input = "";
                term = "";
            }
            // Show
            if (input.equalsIgnoreCase("S")) {
                if (dictionary.isEmpty())
                    System.out.println ("\nThe dictionary is empty.\n");
                else {
                    System.out.println ();
                    System.out.println ("Current Root:");
                    System.out.println ("     " + dictionary.root.toString());
                    System.out.println ();
                    System.out.println ("All entries:");
                    System.out.println (dictionary.toString());
                    System.out.println ();
                }
            }
        }
        
        /*
         * Exit the program
         */
        System.out.println ();
        System.out.println ("-------------------------------------------------");
        System.out.println ("\\\\//_ Live long and prosper.");
    }
}