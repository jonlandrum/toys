Least Common Multiple
=====================

These are a couple of programs for finding the least common multiple of two numbers. The [first one](leastCommonMultiple.cpp) uses Euclid's algorithm (fast), and the [second one](lcm-take-2.cpp) uses an algorithm of my own design.

###### Screenshot
![Screenshot of the finished product](lcm.png "Screenshot of the finished product")
