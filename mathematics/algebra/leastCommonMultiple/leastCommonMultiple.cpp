/*
 *	Programmer:  Jonathan Landrum
 *	Date:        2 March 2012
 *	
 *	Program:     leastCommonMultiple.cpp
 *	Purpose:     Finds the least common multiple
 *	             of two natural numbers using
 *	             Euclid's Algorithm.
 *	
 *	Assumptions: 1.) Input is in natural number
 *	                 form. Real input will be cast
 *	                 to an integer, and negative
 *	                 input will cause a loop until
 *	                 positive input is received.
 */

#include <iostream>

using namespace std;

// --------------------------------------------------
// main():
// --------------------------------------------------
int main () {
	
	// Variables
	int input1 = 0;		// First input variable from user
	int input2 = 0;		// Second input variable from user
	int smaller = 0;	// Smaller of the two input variables
	int larger = 0;		// Larger of the two input variables
	int remainder;		// The remainder from continued modulo
	int result = 0;		// Results returned from the operation
	
	// Introduce the program
	cout << endl;
	cout << "------------------------------------------" << endl;
	cout << "-    Least Common Multiple Calculator    -" << endl;
	cout << "------------------------------------------" << endl;
	cout << endl;
	cout << "This program uses Euclid's Algorithm to" << endl;
	cout << "find the least common multiple of two" << endl;
	cout << "natural numbers." << endl;
	cout << endl;
	
	
	// Get input from user
	cout << "Enter the first natural number: ";
	cin  >> input1;
	cout << endl;
	
	while (input1 < 1) {
		cout << "ERROR: Bad input" << endl;
		cout << "Enter the first natural number: ";
		cin  >> input1;
		cout << endl;
	} // End sanity check
	
	cout << "Enter the second natural number: ";
	cin  >> input2;
	cout << endl;
	
	while (input2 < 1) {
		cout << "ERROR: Bad input" << endl;
		cout << "Enter the second natural number: ";
		cin  >> input2;
		cout << endl;
	} // End sanity check
	
	
	// Find the smaller of the two
	if (input1 - input2 > 0) {
		smaller = input2;
		larger  = input1;
	} else {
		smaller = input1;
		larger  = input2;
	} // End if
	
	if (larger % smaller == 0) {
		result = larger;
	} else {
		remainder = larger % smaller;
		while (remainder != 0) {
			larger = smaller;
			smaller = remainder;
			remainder = larger % smaller;
		} // End while
		result = (input1 * input2) / smaller;
	} // End if
	
	
	// Return the results
	cout << "The least common multiple is " << result << endl << endl;
	
	cout << "\\\\//_ Live long and prosper." << endl;
	return (0);
} // End main
