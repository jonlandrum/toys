/*
 * Programmer:  Jonathan Landrum
 * Date:        13 July 2012
 * 
 * Program:     lcm.cpp    
 * Purpose:     Finds the least common multiple
 *              using an iterative method.
 * Assumptions: None.
 */

#include <iostream>
using namespace std;

// --------------------------------------------------
// main():
// --------------------------------------------------
int main () {
    
    // Variables
    int first  = -1;
    int second = -1;
    int temp   = 0;
    int output = 0;
    
    // Introduce the program
    cout << endl;
    cout << "------------------------------------------" << endl;
    cout << "-    Least Common Multiples, Take Two    -" << endl;
    cout << "------------------------------------------" << endl;
    cout << endl;
    cout << "Finds the Least Common Multiple (LCM)" << endl;
    cout << "of two numbers." << endl;
    cout << endl;
    
    // Get user input, check for sane figures
    while (first < 0) {
        cout << "Enter the first natural number to compare: ";
        cin  >> first;
    }
    while (second < 0) {
        cout << "Enter the second natural number to compare: ";
        cin  >> second;
    }
    cout << endl;
    
    // Cover pre-defined case for 0
    if (first == 0 || second == 0) {
        // Return the results
        cout << 0 << endl;
        cout << "------------------------------------------" << endl;
        cout << "\\\\//_ Live long and prosper." << endl;
        
        return (0);
    }
    
    // Make sure first is the smaller number; goes faster that way
    if (first > second) {
        temp   = first;
        first  = second;
        second = temp;
    }
    
    temp   = first;
    output = second;
    
    // Main processing loop
    while (temp != output) {
        // Don't parse every multiple evar, just the pertinent ones
        while (temp < output)
            temp = temp + first;
        if (temp != output)
            output = output + second;
    }
    
    // Return the results
    cout << output << endl;
    cout << "------------------------------------------" << endl;
    cout << "\\\\//_ Live long and prosper." << endl;
    
    return (0);
} // End main
