Integer Right Triangles
=======================

This program calculates the number of solutions that form a right triangle with integral length sides for perimeter *p* of arbitrary length. For example, for a right triangle with sides {a,b,c} = {3,4,5}, *p* = 3 + 4 + 5 = 12. If *p* = 120, there are exactly three solutions for {a,b,c}: {20,48,52}, {24,45,51}, and {30,40,50}. This program calculates the maximum number of solutions between 12 and *p*, where *p* is input by the user.
