PROGRAM triangle
! ------------------------------------------------------------------------------
! Programmer:     Jonathan Landrum
!
! Purpose:        This program calculates the number of solutions to form a
!                 right triangle with integral length sides for perimeter "p"
!                 of arbitrary length.
!
! Assumptions:    GNU GFortran is installed on the compiling machine
! 
! Revisions:      Date           Programmer          Description of Change
!                 ===========    ================    ========================
!                 18 Jan 2013    Jonathan Landrum    Original code.
! ------------------------------------------------------------------------------
    
    IMPLICIT NONE
    
    ! DATA DICTIONARY: Declared variables
    INTEGER   :: p                  ! Size of the perimeter of the triangle
    INTEGER   :: a, b, c            ! Sides of the triangle
    INTEGER   :: counter            ! Stores the current number of solutions found
    INTEGER   :: maxSolutions       ! Maximum number of solutions for this input
    INTEGER   :: maxPerimeter       ! Perimeter of input with maximum solutions
    INTEGER   :: perimeter          ! The current "input" less than p
    CHARACTER :: response = 'Y'     ! Continue variable
    
    ! Introduce the program
    WRITE (*,*) '* * * * * * * * * * * * * * * * * * * * * * * * * * * * *'
    WRITE (*,*) '*                                                       *'
    WRITE (*,*) '*       Fortran Integer Right Triangle Calculator       *'
    WRITE (*,*) '*                                                       *'
    WRITE (*,*) '* * * * * * * * * * * * * * * * * * * * * * * * * * * * *'
    WRITE (*,*) ''
    WRITE (*,*) '                            |\\'
    WRITE (*,*) '                            | \\'
    WRITE (*,*) '                          a |  \\ c'
    WRITE (*,*) '                            |   \\'
    WRITE (*,*) '                            |____\\'
    WRITE (*,*) '                               b'
    WRITE (*,*) ''
    WRITE (*,*) 'If "p" is the perimeter of a right triangle with integral'
    WRITE (*,*) 'length sides, {a,b,c}, there are exactly three solutions'
    WRITE (*,*) 'for p = 120: {20,48,52}, {24,45,51}, and {30,40,50}.'
    WRITE (*,*) ''
    WRITE (*,*) 'This program calculates the maximum number of solutions'
    WRITE (*,*) 'between 12 and "p" where "p" is arbitrary.'
    WRITE (*,*) ''
    WRITE (*,*) '---------------------------------------------------------'
    WRITE (*,*) ''
    
    ! Continue loop
    DO WHILE (response == 'y' .OR. response == 'Y')
        ! Initialize variables for each pass through the loop
        p = 0
        a = 3
        b = 4
        c = 5
        perimeter = 12
        counter = 0
        maxSolutions = 0
        maxPerimeter = 12
    
        ! Get input from the user
        WRITE (*,'(1X,A39)',advance='no') 'What is the perimeter of the triangle? '
        READ  (*,*) p
        
        ! Do Maths
        DO WHILE (perimeter <= p)
            DO WHILE (c <= p - (a + b))
                DO WHILE (b <= p - (a + c))
                    DO WHILE (a <= p - (b + c))
                        IF (((a * a) + (b * b) == c * c) .AND. (a < b) .AND. ((a + b + c) == perimeter)) THEN
                            !WRITE (*,*) '{', a, ',', b, ',', c, '}'
                            counter = counter + 1
                            IF (counter > maxSolutions) THEN
                                maxSolutions = counter
                                maxPerimeter = perimeter
                            END IF
                        END IF
                        a = a + 1
                    END DO
                    a = 1
                    b = b + 1
                END DO
                b = 1
                c = c + 1
            END DO
            c = 1
            counter = 0
            perimeter = perimeter + 1
        END DO
        
        ! Return results
        WRITE (*,*) 'Maximum solutions: ', maxSolutions
        WRITE (*,*) 'Perimeter for maximum solutions: ', maxPerimeter
        
        ! Prompt the user to continue or exit
        WRITE (*,*) ''
        WRITE (*,'(1X,A49)',advance='no') 'Would you like to calculate another value? [Y/N] '
        READ  (*,*) response
        WRITE (*,*) ''
    
    END DO ! END "continue" loop
    WRITE (*,*) '---------------------------------------------------------'
    WRITE (*,*) '\\\\//_ Live long and prosper.'
END PROGRAM triangle
