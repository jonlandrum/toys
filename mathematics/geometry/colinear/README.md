Colinear
========

This program determines if three points on the [cartesian plane](http://en.wikipedia.org/wiki/Cartesian_coordinate_system) are colinear. It takes six values as command line arguments, and it arranges these as ordered pairs like so:

    argv[1] = x1
    argv[2] = y1
    argv[3] = x2
    argv[4] = y2
    argv[5] = x3
    argv[6] = y3

It then creates three separate [point-slope forms of a linear equation](http://en.wikipedia.org/wiki/Linear_equation#Point.E2.80.93slope_form) using the three points to make three slopes, and it compares these slopes to see if they are equal. If so, the three points are colinear.
