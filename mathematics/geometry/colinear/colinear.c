#include <stdio.h>

float m (int x1, int y1, int x2, int y2) {
    return ((y2 -y1 - 0.0) / (x2 - x1 - 0.0));
}

int main (int argc, char **argv) {
    if (argc < 7) {
        return -1;
    } else {
        int x1 = atoi(argv[1]);
        int y1 = atoi(argv[2]);
        int x2 = atoi(argv[3]);
        int y2 = atoi(argv[4]);
        int x3 = atoi(argv[5]);
        int y3 = atoi(argv[6]);
        
        float m1 = m(x1, y1, x2, y2);
        float m2 = m(x2, y2, x3, y3);
        float m3 = m(x1, y1, x3, y3);
        
        printf ("Slope 1: %f\n", m1);
        printf ("Slope 2: %f\n", m2);
        printf ("Slope 3: %f\n", m3);
        
        printf ("%s\n", m1 == m2 ? "Colinear" : "Not colinear");
    }
}