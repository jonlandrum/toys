Energy
======

This program calculates the total energy of an object that is on Earth by calculating and then adding its potential and kinetic energies.
