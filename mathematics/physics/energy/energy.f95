PROGRAM energy
! ------------------------------------------------------------------------------
! Programmer:     Jonathan Landrum
!
! Purpose:        This program calculates the total energy of an object that is
!                 on Earth by calculating and then adding its potential and 
!                 kinetic energies.
!
! Assumptions:    1.) Input is in metric units.
! 
! Revisions:      Date		Programmer		Description of Change
!                 ===========	=====================	========================
!                 01 Mar 2012	Jonathan Landrum	Original code.
! ------------------------------------------------------------------------------
 
	IMPLICIT NONE
 
	! DATA DICTIONARY: Declared constants
	REAL :: gravity = 9.8		! Gravitational constant in m/s^2
 
	! DATA DICTIONARY: Declared variables
	REAL :: mass			! Mass of the object
	REAL :: height			! Height of the object
	REAL :: velocity		! Velocity of the object
	REAL :: potentialEnergy		! Energy gained from altitude in Joules
	REAL :: kineticEnergy		! Energy gained from motion in Joules
	REAL :: totalEnergy		! Total energy in the object in Joules
 
	! Introduce the program
	WRITE (*,*)
	WRITE (*,*) '* * * * * * * * * * * * * * * * * * * * * * * * * * * * *'
	WRITE (*,*) '*                                                       *'
	WRITE (*,*) '*    Fortran Potential and Kinetic Energy Calculator    *'
	WRITE (*,*) '*                                                       *'
	WRITE (*,*) '* * * * * * * * * * * * * * * * * * * * * * * * * * * * *'
	WRITE (*,*)
	WRITE (*,*) 'This program calculates and adds together the potential'
	WRITE (*,*) 'and kinetic energies of an object on Earth.'
	WRITE (*,*)
	WRITE (*,*) '---------------------------------------------------------'
	WRITE (*,*)
	
	! Get data values from the user
	WRITE (*,*) 'What is the object''s mass in kilograms?'
	READ  (*,*) mass
	WRITE (*,*)
	WRITE (*,*) 'What is the object''s altitude in meters?'
	READ  (*,*) height
	WRITE (*,*)
	WRITE (*,*) 'What is the object''s initial velocity in meters/second?'
	READ  (*,*) velocity
	WRITE (*,*)
	
	! Perform the calculations
	potentialEnergy = mass * gravity * height
	kineticEnergy = 0.5 * mass * velocity ** 2
	totalEnergy = potentialEnergy + kineticEnergy
	
	! Return the results
	WRITE (*,*) 'Potential energy: ', potentialEnergy, ' Joules'
	WRITE (*,*) 'Kinetic energy:   ', kineticEnergy, ' Joules'
	WRITE (*,*) 'Total energy:     ', totalEnergy, ' Joules' 
	WRITE (*,*)
	WRITE (*,*) '\\//_ Live long and prosper.'
END PROGRAM energy
	