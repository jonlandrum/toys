Factorial
=========

These programs demonstrate the [factorial](http://en.wikipedia.org/wiki/Factorial) function. In mathematics, the character `!` is the symbol for the factorial function, and is used after the number, like `10!`. Factorial is defined recursively:

    n! = n * (n - 1)!
    0! = 1

This function, as you might imagine, grows quite rapidly. In the previous example, 10! = 10 * 9 * 8 * 7 * 6 * 5 * 4 * 3 * 2 * 1&nbsp;*&nbsp;1&nbsp;=&nbsp;3,628,800 (It multiplies by 1 twice, once for 1! and once for 0!). There are two implementations of this function included, one in Fortran, and one in GNU Common Lisp. The largest number the Fortran version can calculate is 33!, which equals:

    8,683,317,618,811,886,495,518,194,401,280,000,000

That's quite a large number. The Lisp implementation, however, is able to calculate some much larger values, depending on how much RAM your machine has available. On one machine, I was able to calculate 1,000!:

![1000 Factorial](fact-1000.png "1000 Factorial in GNU Common Lisp")

And on a small supercomputer, I was able to calculate 100,000! over the course of about three minutes. Lisp, FTW.
