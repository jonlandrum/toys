Pascal's Triangle
=================

This program outputs [Pascal's Triangle](http://en.wikipedia.org/wiki/Pascal%27s_triangle) of a specified size.
