// ==================================================
// Programmer:  Jonathan Landrum
// Date:        29 March 2012
// ==================================================
// Program:     pascal.cpp
// Purpose:     Takes user input of an integer and
//		returns a Pascal Triangle of the
//		specified depth.
// Assumptions: 1.) Input is an integer. Real input
//		    will be cast to the next lowest
//		    integer.
//		2.) Input is positive. Negative
//		    input and text input will simply
//		    return nothing.
// Issues:      1.) Output looks normal for row
//		    numbers less than 6. Output is
//		    skewed to the left for row
//		    numbers greater than 5, due to
//		    the introduction of large digits.
//		2.) The function pascal() slows down
//		    significantly for input greater
//		    than 23 due to the fact that it
//		    uses recursion to calculate its
//		    result.
// ==================================================

#include <iostream>
#include <string>

using namespace std;


// --------------------------------------------------
// Function Prototypes
// --------------------------------------------------
int pascal (int row, int column);


// --------------------------------------------------
// main():
// Calls pascal() and returns results.
// --------------------------------------------------
int main () {
	
	// Variables
	float in;
	int   input;
	int   counter;
	int   row;
	int   column;
	int   spaces;
	
	// Introduce the program
	cout << endl;
	cout << "-----------------------------" << endl;
	cout << "-     Pascal's Triangle     -" << endl;
	cout << "-----------------------------" << endl;
	cout << endl;
	cout << "              1" << endl;
	cout << "            1   1" << endl;
	cout << "          1   2   1" << endl;
	cout << "        1   3   3   1" << endl;
	cout << "      1   4   6   4   1" << endl;
	cout << endl;
	cout << "This program asks for an" << endl;
	cout << "integer, and returns a Pascal" << endl;
	cout << "Triangle of that depth." << endl;
	
	// Get user input
	try {
		cout << endl;
		cout << "Enter a natural number for" << endl;
		cout << "the size of your triangle: ";
		
		cin  >> in;
		cout << endl;
		
		input = (int)in;
	}
	catch (...) {
		cout << "Error: << Unexpected Input >>" << endl;
		return (-1);
	}
	
	counter = input;
	
	// Main processing loop
	for (int n = 0; n < input; ++n) {
		cout << "    ";
		
		for (int c = counter; c > 0; --c)
			cout << "  ";
		
		for (int c = 0; c <= n; ++c)
			cout << pascal(n + 1, c + 1) << "   ";
		
		cout << endl;
		--counter;
	}
	
	cout << endl;
	
	return (0);
} // End main


// --------------------------------------------------
// pascal():
//
// Recursive function.
// Returns int, accepts (int row, int column).
// 
// Calculates the value of the number at the 
// specified row and column of a Pascal Triangle.
// --------------------------------------------------
int pascal (int row, int column) {
	int result = 0;
	
	if ((column == 1) || (row == column))
		result = 1;
	else
		result = pascal(row - 1, column - 1) + pascal(row - 1, column);
	
	return result;
}