#include <stdio.h>
#include <stdlib.h>

int main() {
    int ctr=0, ptr=0, mode=0, count=0, tempmd=0, tempct=0;
    float mean=0;
    int arr[100];
    FILE *fp;
    fp=fopen("input.txt", "r");
    while(fscanf(fp, "%d", &arr[ctr]) != EOF) { ++ctr; }
    
    /* Find the mode */
    for(ctr = 0; ctr < 100; ++ctr) {
        tempmd = arr[ctr];
        tempct = 0;
        for(ptr = 0; ptr < 100; ++ptr) {
            if (arr[ptr] == tempmd) { ++tempct; }
        }
        if (tempct > count) {
            count = tempct;
            mode = tempmd;
        }
    }
    
    /* Find the mean */
    for(ctr = 0; ctr < 100; ++ctr) {
        mean += arr[ctr];
    }
    mean /= 100.0;
    
    printf ("%f %d\n", mean, mode);
}

