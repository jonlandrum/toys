Mean & Mode
===========

This program takes a list of natural numbers and finds the <a href="http://en.wikipedia.org/wiki/Arithmetic_mean" title="&ldquo;Arithmetic mean&rdquo; article on Wikipedia">mean</a> and the <a href="http://en.wikipedia.org/wiki/Mode_%28statistics%29" title="&ldquo;Mode (statistics)&rdquo; article on Wikipedia">mode</a> of the collection.
