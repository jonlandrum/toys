Birthday Problem
================

This is a problem that was presented to my Probability and Statistics class a few years ago. The premise is to determine the smallest number of people you would need to poll in order to have a 50% chance of polling two people with the same birthday. Leap years are not considered, and the incidence of twins and other multiples is not considered. Given there are 365 days in our version of the year, you might think the number would be somewhere around 182 people. However, the actual number is much smaller:

![Probability is weird](birthdays.png "Probability is weird")