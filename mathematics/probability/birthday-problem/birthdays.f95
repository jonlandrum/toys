! ---------------------------------------------------------
! Programmer:   Jonathan Landrum
! Date:         20 September 2011
! Class:        MAT 353
! ---------------------------------------------------------
! Program:      Birthdays.exe
! Purpose:      Calculates the number of people you
!               would need to question in order to have
!               a 50% chance in getting two or more
!               duplicate birthdates among the set.
! Assumptions:  None.
! ---------------------------------------------------------

PROGRAM BIRTHDAYS
    IMPLICIT NONE

    REAL    :: probability = 0.0, compliment = 1.0, days = 365
    INTEGER :: counter     = 1

    WRITE (*,*) '******************************************'
    WRITE (*,*) '*                                        *'
    WRITE (*,*) '* FORTRAN DUPLICATE BIRTHDATE CALCULATOR *'
    WRITE (*,*) '*                                        *'
    WRITE (*,*) '******************************************'
    WRITE (*,*)
    WRITE (*,*) 'Calculates the number of people you would'
    WRITE (*,*) 'need to question in order to have a 50/50'
    WRITE (*,*) 'chance in having a duplicate birthdate.'
    WRITE (*,*)

    ! -------------------------------------------------
    ! Main:
    ! This loop begins at 1 with the calculation and
    ! continues until probability is greater than 50%.
    ! -------------------------------------------------
    DO WHILE (probability < 0.51)
        compliment  = compliment * (days/365)
        probability = 1.0 - compliment

        WRITE (*,*) '| ', counter, ' | ', probability, ' |'

        counter = counter + 1
        days    = days - 1
    END DO ! Main loop
    WRITE (*,*)
    WRITE (*,*) '\\//_ Live long and prosper.'
END PROGRAM BIRTHDAYS
