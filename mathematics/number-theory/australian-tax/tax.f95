PROGRAM tax
! ------------------------------------------------------------------------------
! Programmer:     Jonathan Landrum
!
! Purpose:        This program calculates the total taxes owed by an Australian
!                 citizen, based on their taxable income.
!
! Assumptions:    1.) Input is in Real form, truncated to 2 decimal places.
!                     Integer input will be assumed to be even dollars.
! 
! Revisions:      Date		Programmer		Description of Change
!                 ===========	=====================	========================
!                 05 Mar 2012	Jonathan Landrum	Original code.
! ------------------------------------------------------------------------------
 
	IMPLICIT NONE
 
	! DATA DICTIONARY: Declared variables
	REAL    :: income
	REAL    :: taxes = 0
 
	! Introduce the program
	WRITE (*,*)
	WRITE (*,*) '-----------------------------------------'
	WRITE (*,*) 
	WRITE (*,*) '          Fortran Tax Calculator'
	WRITE (*,*) 
	WRITE (*,*) '-----------------------------------------'
	WRITE (*,*)
	WRITE (*,*) 'This program calculates the taxes owed by'
	WRITE (*,*) 'a citizen of Australia, based on income.'
	WRITE (*,*)
	
	! Formats for output
1       FORMAT (1X,A2,1X,F10.2)
	
	! Get data values from the user
	WRITE (*,*) 'What was your income?'
	READ  (*,*) income
	WRITE (*,*)
	
	! Perform the income tax calculations
	IF      (income > 60000) THEN
		taxes = (15580 + (0.47 * (income - 60000)))
	ELSE IF (income > 50000) THEN
		taxes = (11380 + (0.42 * (income - 50000)))
	ELSE IF (income > 20000) THEN
		taxes = (2380 + (0.30 * (income - 20000)))
	ELSE IF (income > 6000) THEN
		taxes = (0.17 * (income - 6000))
	END IF
	
	! Add Medicare to the total
	taxes = taxes + (income * 0.015)
        
	! Return the results 
	WRITE (*,*) 'Your taxes are:'
	WRITE (*,1) 'A$', taxes
	WRITE (*,*)
	WRITE (*,*) '\\//_ Live long and prosper.'
	WRITE (*,*)
END PROGRAM tax