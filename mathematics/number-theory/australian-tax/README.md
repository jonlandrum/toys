Australian Tax Calculator
=========================

This is a programming challenge I found online a few years back and decided to take on one day when I was bored. Essentially, you are given this tax bracket:

![](tax.jpg "Australian tax")

and are asked to write a program that calculates tax for arbitrary input. For example, an input of $120,000 gives a tax rate of $45,580:

![](screenshot.png "Sample run")
