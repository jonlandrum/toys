#include <stdio.h>

int main (int argc, char** argv) {
    int length, cases, enter, exit, min = 3, c, i;

    /* Get the length of the service lane and the number of test cases */
    scanf("%d", &length);
    int width[length];
    scanf("%d", &cases);
    int ans[cases];

    /* Get the width values along the service lane */
    for(c = 0; c < length; ++c) { scanf("%d", &width[c]); }

    /* Loop to read the entry and exit points */
    for (c = 0; c < cases; ++c) {
        scanf("%d", &enter);
        scanf("%d", &exit);
        for (i = enter; i <= exit; ++i) {
            if (width[i] < min) { min = width[i]; }
        }
        ans[c] = min;
        min = 3;
    }
    for (c = 0; c < cases; ++c) { printf("%d\n", ans[c]); }
}
