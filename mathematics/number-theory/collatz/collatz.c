#include <stdio.h>

int collatz (int n);

int main (int argc, char **argv) {
	if (argc < 3) {
		printf ("Error; exiting status -1\n");
		return -1;
	}
	int start;
	int end = atoi(argv[2]);
	int c, temp, max = 0;
	for (start = atoi(argv[1]); start <= end; ++start) {
		temp = start;
		for (c = 1; temp > 1; ++c) {
			temp = collatz(temp);
		}
		if (max < c) { max = c; }
	}
	printf ("%d\n", max);
}

int collatz (int n) {
	if (n == 1) { return 1; }
	if (n % 2 == 0) { return n / 2; }
	else { return 3 * n + 1; }
}