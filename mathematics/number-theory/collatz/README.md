Collatz Conjecture Test
=======================

This program finds the longest chain of operations for  the [Collatz conjecture](http://en.wikipedia.org/wiki/Collatz_conjecture). It takes two [Natural numbers](http://en.wikipedia.org/wiki/Natural_number) as command line arguments. The first number is the start value, and the second is the ending value. The program then checks each number between these two values to see if the string of operations is longer than the current max value, and then outputs this max value upon completion.
