Sum of Primes
=============

This program solves a specific problem: It sums the prime numbers less than 2,000,000.
