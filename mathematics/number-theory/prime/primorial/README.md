Primorial
=========

This program is an implementation of the *[primorial](http://en.wikipedia.org/wiki/Primorial)* function. Primorial is defined similarly to [factorial](http://en.wikipedia.org/wiki/Factorial). But rather than multiplying successive natural numbers, the function multiplies successive prime numbers.
