#include <stdio.h>
#include <math.h>

int isPrime (int n);
int nextPrime (int n);

int main (int argc, char **argv) {
    if (argc == 1) {
        printf ("Usage:\n");
        printf ("\t~> %s [number]\n", argv[0]);
        printf ("where [number] is a natural number.\n");
        return 0;
    }
    int result = 1;
    int prime = 2;
    int counter = atoi(argv[1]);
    while (counter) {
        result *= prime;
        prime = nextPrime (prime);
        --counter;
    }
    printf ("%d\n", result);
    return 0;
}

int isPrime (int n) {
    if (n < 2) { return 0; }
    if (n == 2 || n == 3) { return 1; }
    if (n % 2 == 0) { return 0; }
    int c;
    for (c = 3; c <= sqrt(n); c += 2) { if (n % c == 0) { return 0; } }
    return 1;
}

int nextPrime (int n) {
    if (n == 2) { return 3; }
    if (n % 2 == 0) { n -= 1; }
    n += 2;
    while (!isPrime(n)) {
        n += 2;
    }
    return n;
}