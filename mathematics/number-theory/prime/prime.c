#include <stdio.h>
#include <math.h>

int main (int argc, char** argv) {
    if ((argc < 2) || !strcmp(argv[1], "-h") || !strcmp(argv[1], "--help") || !strcmp(argv[1], "?")) {
        printf ("Prime Number Calculator\n\n");
        printf ("Usage:\n");
        printf ("\t~ %s [number]\n", argv[0]);
        printf ("  where [number] is a positive integer.\n\n");
        printf ("For example:\n");
        printf ("\t~ %s 20\n", argv[0]);
        printf ("  would return \"20 is not prime\", and\n");
        printf ("\t~ %s 43\n", argv[0]);
        printf ("  would return \"43 is prime\".\n");
        return 0;
    }
    printf ("%d %s\n", atoi (argv[1]), isPrime(atoi (argv[1])) ? "is prime." : "is not prime.");
    return 0;
}

int isPrime (int n) {
    if (n < 2) { return 0; }
    if (n == 2 || n == 3) { return 1; }
    if (n % 2 == 0) { return 0; }
    int c;
    for (c = 3; c <= sqrt (n); c += 2) { if (n % c == 0) { return 0; } }
    return 1;
}