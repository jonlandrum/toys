Minimax
=======

This program finds the maximum and minimum largest [prime factors](http://en.wikipedia.org/wiki/Prime_factor) of the set of all zeroless [pandigital numbers](http://en.wikipedia.org/wiki/Pandigital_number) in base 10. This set looks like:

    123456789
    123456798
    123456879
    ...
    987654231
    987654312
    987654321

The program loops through all numbers between 123,456,789 and 987,654,321 inclusive, checks to see if they are pandigital, and then finds the largest prime factor of that number. If the largest prime factor is larger than any other previously recorded prime factor, it is saved as the maximum largest prime factor. If it is smaller than any previous largest prime factor, it is saved as the minimum largest prime factor. At the end, these two values are returned, along with the pandigital number to which they belong.
