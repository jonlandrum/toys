#include <stdio.h>
#include <math.h>

int contains (int array[], int n, int length);
int isPandigital (int n);
int isPrime (int n);
int nextPrime (int n);

int main (int argc, char **argv) {
    int c, temp, prime, max = 0, maxPandigital, minmax = -1, minmaxPandigital;
    for (c = 123456789; c < 987654322; ++c) {
        if (isPandigital(c)) {
            temp = c, prime = 2;
            while (temp > 1) {
                while (temp % prime == 0) { temp /= prime; }
                prime = nextPrime(prime);
            }
            if (prime > max) {
                max = prime;
                maxPandigital = c;
            }
            if ((minmax > prime) || (minmax < 0)) {
                minmax = prime;
                minmaxPandigital = c;
            }
        }
    }
    printf ("Maximum maxima prime factor: %d\n", max);
    printf ("Pandigital number for max: %d\n", maxPandigital);
    printf ("Minimum maxima prime factor: %d\n", minmax);
    printf ("Pandigital number for minmax: %d\n", minmaxPandigital);
}

int contains (int array[], int n, int length) {
    int c;
    for (c = 0; c < length; ++c) {
        if (array[c] == n) { return 1; }
    }
    return 0;
}

int isPandigital (int n) {
    int length = 0;
    int array[9];
    int c;
    for (length = 0; n > 0; ++length) {
        if (length == 9 && n > 0) { return 0; }
        if (n == 0 && length < 9) { return 0; }
        array[length] = n % 10;
        n /= 10;
    }
    for (c = 1; c <= 9; ++c) {
        if (!contains(array, c, 9)) {
            return 0;
        }
    }
    return 1;
}

int isPrime (int n) {
    if (n < 2) { return 0; }
    if (n == 2 || n == 3) { return 1; }
    if (n % 2 == 0) { return 0; }
    int c;
    for (c = 3; c <= sqrt (n); c += 2) { if (n % c == 0) { return 0; } }
    return 1;
}

int nextPrime (int n) {
    if (n == 2) { return 3; }
    if (n % 2 == 0) { n -= 1; }
    n += 2;
    while (!isPrime(n)) {
        n += 2;
    }
    return n;
}
