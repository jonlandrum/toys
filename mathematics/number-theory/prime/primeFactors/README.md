Largest Prime Factor
====================

This program finds the largest prime factor of a given number.

![Example run of the program](screenshot.png "Example run of the program")
