class Integer
    def prime?
        if (self <= 1) then
            return false
        elsif ((self == 2) or (self == 3)) then
            return true
        elsif (self % 2 == 0) then
            return false
        else
            c = 3
            begin
                if (self % c == 0) then
                    return false
                end
                c += 2
            end while (c * c < self)
            return true
        end
    end
end

c = 2

File.open('primes.txt', 'w') do |f|
    until (c > 1000000000) do
        if (c.prime?) then
            f.write(c)
            f.write("\n")
        end
        if (c == 2) then
            c += 1
        else
            c += 2
        end
    end
end