#include <stdio.h>

int main (int argc, char** argv) {
    if (argc < 11) {
        printf ("\nAmino Acid Comparitor\n\n");
        printf ("Usage:\n");
        printf ("\t~> %s [numbers]\n", argv[0]);
        printf ("where [numbers] is a list of ten integers.\n\n");
        printf ("The numbers represent the number of atoms of this list of");
        printf (" elements in this order:\n\n");
        printf ("/==========================\\\n");
        printf ("| Element  | Atomic Weight |\n");
        printf ("|==========================|\n");
        printf ("| Carbon   | 12.011        |\n");
        printf ("|--------------------------|\n");
        printf ("| Hydrogen | 1.00794       |\n");
        printf ("|--------------------------|\n");
        printf ("| Nitrogen | 14.00674      |\n");
        printf ("|--------------------------|\n");
        printf ("| Oxygen   | 15.9994       |\n");
        printf ("|--------------------------|\n");
        printf ("| Sulfur   | 32.066        |\n");
        printf ("\\==========================/\n\n");
        printf ("The inputs expected are five integers for the first molecule ");
        printf ("and five for the second. The program will compare their ");
        printf ("respective masses and output the difference.\nFor example:\n");
        printf ("\t~> %s 6 13 1 2 0 4 9 1 3 0\n", argv[0]);
        printf ("will output:\n");
        printf ("\t~> The molecular weight of the first amino acid is 131.17\n");
        printf ("\t~> The molecular weight of the second amino acid is 119.12\n");
        printf ("\t~> The difference in weights is 12.05\n");
    } else {
        int c;
        float mol1 = 0, mol2 = 0;
        float weights[5] = {12.011, 1.00794, 14.00674, 15.9994, 32.066};
        for (c = 0; c < 5; ++c) {
            mol1 += atoi(argv[c + 1]) * weights[c];
        }
        printf ("The molecular weight of the first amino acid is %.2f\n", mol1);
        for (c = 5; c < 10; ++c) {
            mol2 += atoi(argv[c + 1]) * weights[c % 5];
        }
        printf ("The molecular weight of the second amino acid is %.2f\n", mol2);
        printf ("The difference in weights is %.2f\n", mol1 - mol2 < 0 ? mol2 - mol1 : mol1 - mol2);
    }
}