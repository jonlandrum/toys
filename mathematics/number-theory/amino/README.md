Amino Acid Mass Difference Calculator
=====================================

This program takes a list of ten integers as command line arguments which represent the numbers of atoms in this list of elements in this order:

| Element | Atomic Weight |
| --------- | --------- |
| Carbon  | 12.011 |
| Hydrogen | 1.00794 |
| Nitrogen | 14.00674 |
| Oxygen | 15.9994 |
| Sulfur | 32.066 |

Carbon is listed first, and the remainder are listed in atomic order. The inputs expected are five integers for the first molecule and five for the second. The program will compare their respective masses and output the difference. For example, this input:

    ~> ./amino 6 13 1 2 0 4 9 1 3 0
    
will output:

    ~> The molecular weight of the first amino acid is 131.17
    ~> The molecular weight of the second amino acid is 119.12
    ~> The difference in weights is 12.05
