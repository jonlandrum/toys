Maximum Exclusive Or
====================

This program finds the maximum value of the [exclusive-or operation](http://en.wikipedia.org/wiki/Exclusive_or) of every pair of numbers between two bounds.
