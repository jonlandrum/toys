#include <stdio.h>

int main (int argc, char **argv) {
    int ctr1, ctr2, lim1, lim2, max = 0;
    scanf ("%d", &lim1);
    scanf ("%d", &lim2);
    for (ctr1 = lim1; ctr1 <= lim2; ++ctr1) {
        for (ctr2 = lim1; ctr2 <= lim2; ++ctr2) {
            int temp = ctr1 ^ ctr2;
            if (temp > max) { max = temp; }
        }
    }
    printf ("%d\n", max);
}
