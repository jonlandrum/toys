#include <iostream>
#include "gmp.h"

using namespace std;

int main() {
	mpz_t      oldNumber;
	mpz_t      currentNumber;
	mpz_t      nextNumber;
	mpz_init   (oldNumber);
	mpz_init   (currentNumber);
	mpz_init   (nextNumber);
	mpz_set_si (oldNumber, 1);
	mpz_set_si (currentNumber, 1);
	
	signed long int current;
	signed long int next;
	
	while (currentNumber > 0) {
		cout << currentNumber << endl;
		mpz_add (nextNumber, currentNumber, oldNumber);
		
		current = (long)currentNumber;
		next = (long)nextNumber;
		
		mpz_set_si (oldNumber, current);
		mpz_set_si (currentNumber, next);
	}
	return 0;
}