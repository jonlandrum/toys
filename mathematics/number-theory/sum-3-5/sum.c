#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>

int main () {
    int64_t T;
    scanf("%"PRId64"", &T);
    int64_t arr[T];
    int64_t n;
    for (n = 0; n < T; ++n) {
        scanf("%"PRId64"", &arr[n]);
    }
    for (n = 0; n < T; ++n) {
        int64_t sum = 0;
        int64_t x = 0;
        while (1) {
            x += 3; if (x >= arr[n]) { break; }
            sum += x;
            x += 2; if (x >= arr[n]) { break; }
            sum += x;
            ++x; if (x >= arr[n]) { break; }
            sum += x;
            x += 3; if (x >= arr[n]) { break; }
            sum += x;
            ++x; if (x >= arr[n]) { break; }
            sum += x;
            x += 2; if (x >= arr[n]) { break; }
            sum += x;
            x += 3; if (x >= arr[n]) { break; }
            sum += x;
        }
        printf ("%"PRId64"\n", sum);
    }
}
