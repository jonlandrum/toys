class Integer
    def f
        return self <= 1 ? 1 : (self - 1).f + (self - 2).f
    end
end

c = 1

while c < 30 do
    puts (c.f / ((c - 1).f).to_f)
    c += 1
end