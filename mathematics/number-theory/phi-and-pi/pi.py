#! /usr/local/bin/python3

def divide(int):
    "Does the actual calculation"
    return 4.0 / (int * (int + 1) * (int + 2))

answer = 3.0
c = 2
i = 1

while c < 1000000:
    if i % 2 == 0:
        answer = answer - divide(c)
        print answer
        c += 2
    else:
        answer = answer + divide(c)
        print answer
        c += 2
    i += 1