Prolog peg jump
===============

A Prolog implementation of a linear peg jump game.

        (R) (R) (R) (R)     (B) (B) (B) (B)
         |   |   |   |       |   |   |   |
       __|___|___|___|_______|___|___|___|_
      /  |   |   |   |   _   |   |   |   | \
     /______________________________________\
    |________________________________________|

This program outputs all successful ways to play the game.
