Matrix parenthesization
=======================

This program finds the most efficient parenthesization for multiplying a given set of matrices. It doesn't actually do the multiplication for two reasons: 1.) That part is trivial to code (the point of this project was the challenge), and 2.) It would take the user a very long time to type all the numbers in for each matrix.
