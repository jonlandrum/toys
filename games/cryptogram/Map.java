import java.util.Random;

public class Map {
    private char[] map = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
                          'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
    private String ciphertext = "";
    private String cleartext = "";
    private void shuffleMap() {
        for (int i = 0; i < 10000; ++i) {
            Random rand1 = new Random();
            int num1 = rand1.nextInt(2147483647) % 26;
            Random rand2 = new Random();
            int num2 = rand2.nextInt(2147483647) % 26;
            while (num1 == num2) {
                num2 = rand2.nextInt(2147483647) % 26;
            }
            char temp = map[num1];
            map[num1] = map[num2];
            map[num2] = temp;
        }
    }
    private void encrypt() {
        String s = "";
        for (int i = 0; i < cleartext.length(); ++i) {
            char c;
            if ((Character.toUpperCase(cleartext.charAt(i)) - 65) < 0 ||
                (Character.toUpperCase(cleartext.charAt(i)) - 65) > 25) {
                c = cleartext.charAt(i);
            } else {
                c = map[Character.toUpperCase(cleartext.charAt(i)) - 65];
            }
            s = s + c;
        }
        ciphertext = s;
    }

    public Map() {
        shuffleMap();
        for (int i = 0; i < 10000; ++i) {
            Random rand = new Random();
            int num = rand.nextInt(2147483647) % 5;
            switch(num) {
                case 0:
                    cleartext = "The quick, brown fox jumped over the lazy dog.";
                    break;
                case 1:
                    cleartext = "My new password is \"correct horse battery staple\"";
                    break;
                case 2:
                    cleartext = "The snow this year is better at Innsbrook.";
                    break;
                case 4:
                    cleartext = "It was the best of times, it was the worst of times...";
                    break;
                case 5:
                    cleartext = "Strange weather we are having this year.";
                    break;

            }
        }
        encrypt();
    }
    public Map(String s) {
        shuffleMap();
        cleartext = s;
        encrypt();
    }
    public String getCiphertext() {
        return ciphertext;
    }
    public String solve(String s) {
        int j = 0;
        for (int i = 0; i < s.length(); ++i) {
            char c;
            if ((Character.toUpperCase(s.charAt(i)) - 65) < 0 ||
                (Character.toUpperCase(s.charAt(i)) - 65) > 25) {
                while ((Character.toUpperCase(s.charAt(i)) - 65) < 0 ||
                       (Character.toUpperCase(s.charAt(i)) - 65) > 25) {
                    ++i;
                }
                if (i == s.length()) break;
            }
            if ((Character.toUpperCase(cleartext.charAt(j)) - 65) < 0 ||
                (Character.toUpperCase(cleartext.charAt(j)) - 65) > 25) {
                while ((Character.toUpperCase(cleartext.charAt(j)) - 65) < 0 ||
                       (Character.toUpperCase(cleartext.charAt(j)) - 65) > 25) {
                    ++j;
                }
                if (j == cleartext.length()) break;
            }
            if (Character.toUpperCase(s.charAt(i)) != Character.toUpperCase(cleartext.charAt(j))) {
                return "Incorrect solution. Try again:";
            }
            ++j;
        }
        return "Correct!";
    }
}
