Cryptogram
==========

This is sort of a combination tool and game. You can either encrypt a cleartext string into a ciphertext to be decrypted by hand, or you can take a stab at one of the default strings. If you decrypt the string correctly, you win!