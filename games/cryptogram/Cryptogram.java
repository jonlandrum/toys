import java.util.Scanner;

public class Cryptogram {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Do you want a random string to decode? ");
        Map map;
        if (scan.next().toUpperCase().charAt(0) == 'Y') {
            scan.nextLine();
            map = new Map();
            System.out.println();
            System.out.println(map.getCiphertext());
            System.out.println();
            System.out.print("Solution: ");
            String sol = scan.nextLine();
            String ans = map.solve(sol);
            System.out.println();
            System.out.println(ans);
            while (ans.toUpperCase().charAt(0) != 'C') {
                sol = scan.nextLine();
                ans = map.solve(sol);
                System.out.println(ans);
            }
        } else {
            scan.nextLine();
            System.out.println("Enter a string to encode:");
            map = new Map(scan.nextLine());
            System.out.println();
            System.out.println(map.getCiphertext());
        }
    }
}
